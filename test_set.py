#!/usr/bin/python3

import pytest
from honey_money import *


def test_new_account():
	acc = Account("USD")
	assert acc.get_currency() == "USD"
	assert acc.get_amount() == 0
	acc = Account("RUB")
	assert acc.get_currency() == "RUB"
	assert acc.get_amount() == 0
	#acc = Account("XXX")
	#assert acc == None

def test_depositing():
	acc = Account("RUB")
	acc.put(100, "RUB")
	assert acc.get_amount() == 100
	acc.put(10, "USD")
	assert acc.get_amount() == 855
	assert acc.get_currency() == "RUB"

def test_withdrawal():
	acc = Account("USD")
	acc.put(50, "USD")
	assert acc.get(5, "USD")
	assert acc.get_amount() == 45
	assert acc.get(1510, "RUB")
	assert acc.get_amount() == 25
	assert acc.get(30, "USD") == -1
	assert acc.get_amount() == 25

def test_transfer():
	acc1 = Account("USD")
	acc1.put(50, "USD")
	acc2 = Account("USD")
	acc3 = Account("EUR")
	acc1.transfer(acc2, 10, "USD")
	assert acc1.get_amount() == 40
	assert acc1.get_amount() == 10
	acc1.transfer(acc3, 12, "USD")
	assert acc1.get_amount() == 28
	assert acc1.get_amount() == 10
