# TODO

TODO файл

## General task

Применяя TDD, разработать мультивалютный счёт с возможностью снятия/внесения средств, перевода между счетами.

## Tasks

Желаемая функциональность:

- [ ] Вывод списка доступных валют

- [ ] Создание счета с проверкой наличия запрошенной номинальной валюты

- [ ] Внесение/снятие средств в различных валютах

- [ ] Переводы между счетами с конвертацией валют

## Steps

- [x] Написать TODO

- [ ] Написать тесты для текущих задач

- [ ] Реализовать класс Account

- [ ] Реализовать класс Currency

